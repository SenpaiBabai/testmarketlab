import { ApiProperty } from '@nestjs/swagger';

export class CreateLinkDto {
  @ApiProperty({
    description: 'Значение нужное для создания/получения одноразовой ссылки',
    example: 'Some sting',
  })
  value: string;
}
