import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { provideDatabaseConfig } from './mongo.config';
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (): MongooseModuleOptions => provideDatabaseConfig(),
    }),
  ],
  exports: [MongooseModule],
})
export class MongoModule {}
