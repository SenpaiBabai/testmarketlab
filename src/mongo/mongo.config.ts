import { MongooseModuleOptions } from '@nestjs/mongoose';

export interface MongoConfig {
  tls?: boolean;
  ssl?: boolean;
  tlsCertificateKeyFile?: string;
  tlsCAFile?: string;
  sslCA?: string;
  replicaSet?: string;
  uri: string;
}

export const provideDatabaseConfig = (): MongooseModuleOptions => {
  const config: MongooseModuleOptions = {
    tls: process.env.MONGODB_TLS === 'true',
    ssl: process.env.MONGODB_SSL === 'true',
    uri: 'mongodb://',
    autoIndex: true,
  };

  if (config.tls) {
    config.uri += `${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`;
    config.tlsCAFile = process.env.MONGODB_TLS_CA_FILE;
    config.tlsCertificateKeyFile = process.env.MONGODB_TLS_CRT_KEY;
  } else {
    config.uri += `${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOST}`;
    if (process.env.MONGODB_PORT && process.env.MONGODB_PORT.length > 0) {
      config.uri += `:${process.env.MONGODB_PORT}`;
    }
    config.uri += `/${process.env.MONGODB_DATABASE}`;
    if (
      process.env.MONGODB_AUTH_SOURCE &&
      process.env.MONGODB_AUTH_SOURCE.length > 0
    )
      config.uri += `?authSource=${process.env.MONGODB_AUTH_SOURCE}`;
  }

  if (config.ssl || config.tls) {
    config.ssl = true;
    config.tls = true;
  }

  return config;
};
