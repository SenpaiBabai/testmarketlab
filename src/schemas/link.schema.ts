import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Link {
  @Prop({ required: true })
  value: string;

  @Prop({ required: true, unique: true })
  token: string;

  @Prop({ default: true })
  active: boolean;
}

export const LinkSchema = SchemaFactory.createForClass(Link);
export const LinkSchemaName = 'link_schema';

export type LinkDocument = Link & Document;

export const LinkModel = {
  name: LinkSchemaName,
  schema: LinkSchema,
};
