import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { LinkModule } from './link/link.module';
import { MongoModule } from './mongo/mongo.module';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), MongoModule, LinkModule],
})
export class AppModule {}
