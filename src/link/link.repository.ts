import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LinkDocument, LinkSchemaName } from 'src/schemas/link.schema';

@Injectable()
export class LinkRepository {
  constructor(
    @InjectModel(LinkSchemaName) private linkModel: Model<LinkDocument>,
  ) {}

  async createLink(value: string, token: string): Promise<LinkDocument> {
    const createdLink = new this.linkModel({ value, token });
    return createdLink.save();
  }

  async findLinkByToken(token: string): Promise<LinkDocument> {
    return this.linkModel.findOne({ token }).exec();
  }

  async deactivateLink(link: LinkDocument): Promise<LinkDocument> {
    link.active = false;
    return link.save();
  }
}
