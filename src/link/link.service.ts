import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { LinkRepository } from './link.repository';

@Injectable()
export class LinkService {
  constructor(private readonly linkRepository: LinkRepository) {}

  async createLink(value: string): Promise<string> {
    const token = uuidv4();
    await this.linkRepository.createLink(value, token);
    return token;
  }

  async getLink(token: string): Promise<string> {
    const link = await this.linkRepository.findLinkByToken(token);
    if (!link || !link.active) {
      throw new NotFoundException('Link not found or already used');
    }
    await this.linkRepository.deactivateLink(link);
    return link.value;
  }
}
