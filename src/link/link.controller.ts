import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { LinkService } from './link.service';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { CreateLinkDto } from 'src/dto/link.dto';

@ApiTags('links')
@Controller('link')
export class LinkController {
  constructor(private readonly linkService: LinkService) {}

  @ApiOperation({ summary: 'Создать одноразовую ссылку' })
  @ApiResponse({ status: 201, description: 'Ссылка успешно создана.' })
  @Post()
  async createLink(@Body() createLinkDto: CreateLinkDto): Promise<string> {
    const token = await this.linkService.createLink(createLinkDto.value);
    return `http://localhost:3000/link/${token}`;
  }

  @ApiOperation({ summary: 'Получить значение по одноразовой ссылке' })
  @ApiResponse({ status: 200, description: 'Значение успешно получено.' })
  @ApiResponse({
    status: 404,
    description: 'Ссылка не найдена или уже использована.',
  })
  @Get(':token')
  async getLink(@Param('token') token: string): Promise<string> {
    return this.linkService.getLink(token);
  }
}
