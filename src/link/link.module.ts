import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LinkService } from './link.service';
import { LinkController } from './link.controller';
import { LinkModel } from 'src/schemas/link.schema';
import { LinkRepository } from './link.repository';

@Module({
  imports: [MongooseModule.forFeature([LinkModel])],
  controllers: [LinkController],
  providers: [LinkService, LinkRepository],
})
export class LinkModule {}
